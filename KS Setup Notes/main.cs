﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Linq;
using VMS.TPS.Common.Model.API;
using VMS.TPS.Common.Model.Types;


namespace VMS.TPS
{
    public class Script
    {

        public Script()
        {
        }

        public void Execute(ScriptContext context)
        {
            MessageBox.Show("Hello, thank you for beta testing this script \nI suspect it has errors so please double check eveything \nThis should only be used for triangulated, non-DIBH cases for now.\nPlease let Karl know of any catastrophic issues.\nTo get your plan notes just press Enter and then Copy the next box with Ctrl+C\nSSDs improperly calculated for shifted plans: will patch ASAP");
            string setup_notes = "";
            Image ct = context.Image;
            MessageBox.Show(ct.Id); 

            Structure body = context.StructureSet.Structures.First(x => x.Id.ToLower().Replace(" ", "") == "body");

            PlanSetup ps = context.PlanSetup;
            StructureSet ss = context.StructureSet;
            VVector iso = round_vvector(ps.Beams.First().IsocenterPosition);
            VVector tri = round_vvector(ct.UserOrigin);
            Tuple<double, double, double> ssd = calc_ssd(ct, body, iso, ps); //tuple order is 0/90/270

            bool is_dibh = _is_dibh(ct);
            bool is_vmat = _is_vmat(ps);
            bool is_bolus = _is_bolus(ps, ss);




            bool iso_equals_tri = false;
            if (vvector_equals(iso, tri))
            {
                iso_equals_tri = true;
            }

            bool is_orthogs = (check_for_beam_at_angle(ps, 180) | check_for_beam_at_angle(ps, 0)) & (check_for_beam_at_angle(ps, 90) | check_for_beam_at_angle(ps, 270));

            if (iso_equals_tri)
            {
                setup_notes += "Iso = Tri Pt \n";
            }

            if (iso_equals_tri & is_orthogs)
            {
                setup_notes = "Iso= Tri Pt (Already on the Short Summary, no need to enter in comments) \n";
            }

            if (iso_equals_tri & !is_orthogs)
            {
                setup_notes += String.Format("SSDs @ Iso \ng0 SSD = {0:0.00} cm \ng90 SSD = {1:0.00} cm \ng270 SSD = {2:0.00} cm\n", ssd.Item1, ssd.Item2, ssd.Item3);

            }

            if (!iso_equals_tri)
            {
                VVector beam_iso = ps.Beams.First().IsocenterPosition;
                beam_iso = ct.DicomToUser(beam_iso, ps);
                double shift_x = beam_iso.x * 0.1; string left_right = "";
                double shift_y = beam_iso.y * 0.1; string ant_post = "";
                double shift_z = beam_iso.z * 0.1; string sup_inf = "";

                if (shift_x > 0)
                {
                    left_right = "Left";
                }
                else
                {
                    left_right = "Right";
                }

                if (shift_y > 0)
                {
                    ant_post = "Post";
                }
                else
                {
                    ant_post = "Ant";
                }

                if (shift_z > 0)
                {
                    sup_inf = "Sup";
                }
                else
                {
                    sup_inf = "Inf";
                }

                shift_x = Math.Abs(shift_x);
                shift_y = Math.Abs(shift_y);
                shift_z = Math.Abs(shift_z);
                double ssd_0 = ssd.Item1 - shift_y;
                double ssd_90 = ssd.Item2 - shift_x;
                double ssd_270 = ssd.Item3 + shift_x;

                setup_notes += String.Format("Trinagulate Patient\nSSDs @ Iso \ng0 SSD = {0:0.0} cm \ng90 SSD = {1:0.0} cm \ng270 SSD = {2:0.0} cm\n", ssd_0, ssd_90, ssd_270);
                setup_notes += String.Format("\n\nShift Isocenter\n{0:0.00} {1}\n{2:0.00} {3}\n{4:0.00} {5}", shift_x, left_right, shift_y, ant_post, shift_z, sup_inf);

            }

            if (is_dibh) { setup_notes += "Looks like this plan is on a DIBH scan \n"; }
            if (is_vmat) { setup_notes += "\nVMAT: Rails In \n"; }
            if (is_bolus) { setup_notes += "\nPlease enter bolus info (coming in update) \n"; }

            MessageBox.Show(setup_notes);

        }


        //Bunch of methods to make stuff work
        bool check_for_beam_at_angle(PlanSetup ps, int angle)
        {
            foreach (Beam b in ps.Beams)
            {
                if (b.ControlPoints.First().GantryAngle == angle)
                {
                    return true;
                }
            }
            return false;
        }
        public VVector round_vvector(VVector v)
        {
            v.x = Math.Round(v.x);
            v.y = Math.Round(v.y);
            v.z = Math.Round(v.z);
            return v;

        }

        public bool vvector_equals(VVector x, VVector y)
        {
            double xx = Math.Abs(x.x - y.x);
            double yy = Math.Abs(x.y - y.y);
            double zz = Math.Abs(x.z - y.z);

            double t = xx + yy + zz;

            if (t > 0.01)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public Tuple<double, double, double> calc_ssd(Image ctSim, Structure body, VVector user_origin, PlanSetup ps)
        {
            //USER ORIGIN PARAMETER NAME IS DEPRECATED, SSD CALC'S AT **ISO**
            // order is 0, 90, 270
            //posterior is +
            //left is +
            double ssd0;
            double ssd90;
            double ssd270;

            VVector ap = user_origin;
            VVector lat90 = user_origin;
            VVector lat270 = user_origin;

            if (ctSim.ImagingOrientation.ToString().ToLower().Contains("prone"))
            {
                while (body.IsPointInsideSegment(ap))
                {
                    ap.y += .1;
                }

                while (body.IsPointInsideSegment(lat90))
                {
                    lat90.x -= .1;
                }

                while (body.IsPointInsideSegment(lat270))
                {
                    lat270.x += .1;
                }
            }

            else if ((ctSim.ImagingOrientation.ToString().ToLower().Contains("sup")))

            {
                while (body.IsPointInsideSegment(ap))
                {
                    ap.y -= .1;
                }
                //VVector tmp = ctSim.DicomToUser(ap,ps); MessageBox.Show(tmp.y.ToString());
                while (body.IsPointInsideSegment(lat90))
                {
                    lat90.x += .1;
                }
                //tmp = ctSim.DicomToUser(lat90, ps); MessageBox.Show(tmp.x.ToString());
                while (body.IsPointInsideSegment(lat270))
                {
                    lat270.x -= .1;
                }
                // tmp = ctSim.DicomToUser(lat270, ps); MessageBox.Show(tmp.x.ToString());


            }

            else
            {
                MessageBox.Show("It looks like this patient is head first, don't use this script");
            }
            ap = ctSim.DicomToUser(ap, ps);
            lat90 = ctSim.DicomToUser(lat90, ps);
            lat270 = ctSim.DicomToUser(lat270, ps);
            ssd0 = 0.1 * (1000.0 - Math.Abs(ap.y));
            ssd90 = 0.1 * (1000.0 - Math.Abs(lat90.x));
            ssd270 = 0.1 * (1000.0 - Math.Abs(lat270.x));

            return Tuple.Create(ssd0, ssd90, ssd270);

        }

        public int get_z_index_for_point(VVector point, Image ctSim, Structure body)
        {
            VVector[][] tmp = body.GetContoursOnImagePlane(0);
            VVector z_0 = tmp[0][0];
            double z = point.z; z = Math.Abs(z_0.z) - Math.Abs(z);
            double z_mm = ctSim.ZRes;

            return (int)Math.Round(z / z_mm);
            //THIS PROBABLY ONLY WORKS AS INTENDED FOR HEAD FIRST!!

        }


        public bool _is_dibh(Image ct)
        {
            bool is_dibh = false;
            if (ct.Id.ToLower().Contains("DIBH"))
            {
                is_dibh = true;
            }
            return is_dibh;
        }

        public bool _is_vmat(PlanSetup ps)
        {
            foreach (Beam b in ps.Beams)
            {
                if (b.IsSetupField) { continue; }
                if (b.ControlPoints.First().GantryAngle != b.ControlPoints.Last().GantryAngle)
                {
                    return true;
                }
            }
            return false;
        }

        public bool _is_bolus(PlanSetup ps, StructureSet ss)
        {
            foreach (Beam b in ps.Beams)
            {
                if (b.Boluses.Count() > 0) { return true; }
            }
            foreach (Structure s in ss.Structures)
            {
                if (s.Id.ToLower().Contains("bolus")) { return true; }
            }
            return false;
        }


    }
}
